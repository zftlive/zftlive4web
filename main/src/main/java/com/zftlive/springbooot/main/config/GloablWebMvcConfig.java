//package com.zftlive.springbooot.main.config;
//
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
//
///**
// * 全局静态资源映射处理类,默认支持resources/static/public/webjars(从webJar库下载jar内置目录) 优先级：resources>static>public
// *
// * @author zengfantian
// * @version 1.0
// */
//@Configuration
//public class GloablWebMvcConfig implements WebMvcConfigurer {
//
//    /**
//     * 注意，此处会覆盖默认支持的四个目录静态资源映射
//     *
//     * @param registry
//     */
//    @Override
//    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//        //工程/resource/static目录下的所有文件
//        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
////        // 本地磁盘目录下的所有文件 ( Win: D:/IdeaProjects/forum/images/  linux: /www/wwwroot/images/)
////        registry.addResourceHandler("/static/**").addResourceLocations("file:/usr/local/opt");
//
//    }
//}

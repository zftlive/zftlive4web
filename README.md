## 工程介绍

记录服务端开发学习碎片，并逐步完善、优化

## 工程结构

- main 主应用入口模块

- package.json 版本相关说明


## 事项说明

1、打包请忽略掉tests代码，使用命令：mvn clean install package '-Dmaven.test.skip=true'

2、工程有任何问题请联系作者<zftlivee@163.com>



## 特别说明

本地启动源码工程需注意以下几点：

1、请将Maven-Profiles勾选成local

2、如果没有生效，将application.yml配置文件的@activeProfile@强制设置成local（但是别提交到git仓库）

3、本地环境连接自己本地数据的自行调整数据库连接url和账号密码 